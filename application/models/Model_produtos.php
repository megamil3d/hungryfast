<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_produtos extends MY_Model {

		//Listar
		public function view_subcategorias(){

			return array('subcategoria' => $this->db->query("select cad_subcategorias.*, round(preco,2) as preco, categoria from cad_subcategorias inner join cad_categorias on id_categoria = fk_categoria")->result(),
				'categorias' => $this->db->get("cad_categorias")->result());

		}

		public function exibirSubcategoria($id){
			return array('subcategoria' => $this->db->query("select cad_subcategorias.*, round(preco,2) as preco, categoria from cad_subcategorias inner join cad_categorias on id_categoria = fk_categoria where id_subcategoria = {$id}")->row(),
				'categorias' => $this->db->get("cad_categorias")->result());
		}

		//Listar
		public function view_categorias(){

			return $this->db->get("cad_categorias")->result();

		}

		public function exibircategoria($id){
			return $this->db->get_where("cad_categorias",array('id_categoria'=>$id))->row();
		}

		//Listar
		public function view_produtos(){

			return array('produtos' => $this->db->query("select cad_produtos.*, round(preco,2) as preco, ifnull((select count(*) from elo_produto_subcategorias where fk_produto = id_produto),0) itens from cad_produtos where fk_usuario is null")->result(),
				'categorias' => $this->db->query("
							SELECT 
								id_subcategoria as id, 
							    concat(categoria,' | ',subcategoria) as opcao
							    	from cad_categorias
							        	inner join cad_subcategorias on id_categoria = fk_categoria")->result()
			);

		}

		public function exibirProduto($id){
			return array(
				'produto' => $this->db->select("cad_produtos.*, replace(round(preco,2),'.',',') as preco")
										->get_where("cad_produtos",array('id_produto'=>$id))->row_array(),
				'elo' => $this->db->get_where("elo_produto_subcategorias",array('fk_produto'=>$id))->result_array(),
				'categorias' => $this->db->query("
							SELECT 
								id_subcategoria as id, 
							    concat(categoria,' | ',subcategoria) as opcao
							    	from cad_categorias
							        	inner join cad_subcategorias on id_categoria = fk_categoria")->result()
			);
		}


		//Criar
		public function add_subcategorias($valores = null){

			$this->db->insert('cad_subcategorias',$valores);
			return $this->db->insert_id();

		}

		//Criar
		public function add_categorias($valores = null){

			$this->db->insert('cad_categorias',$valores);
			return $this->db->insert_id();

		}

		//Criar
		public function add_produtos($valores = null){

			$this->db->insert('cad_produtos',$valores);
			return $this->db->insert_id();

		}

		public function limparItens($id_produto){
			$this->db->where('fk_produto', $id_produto);
			$this->db->delete('elo_produto_subcategorias');
		}

		public function inserirItens($id,$item){
			$this->db->query('insert into elo_produto_subcategorias (fk_subcategoria,fk_produto) 
								values ('.$item.','.$id.')');
		}


		//Editar
		public function editar_subcategorias($valores){

			$this->db->where(array('id_subcategoria' => $valores['id_subcategoria']));
			$this->db->update('cad_subcategorias',$valores);

		}

		//Editar
		public function editar_categorias($valores){

			$this->db->where(array('id_categoria' => $valores['id_categoria']));
			$this->db->update('cad_categorias',$valores);

		}

		//Editar
		public function editar_produtos($valores){

			$this->db->where(array('id_produto' => $valores['id_produto']));
			$this->db->update('cad_produtos',$valores);

		}


	}