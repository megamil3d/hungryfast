<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_webservice extends MY_Model {

		public function validar_login($valores){

			$this->db->select('id_usuario, nome_usuario, email_usuario, fk_grupo_usuario, ativo_usuario');
			$this->db->from('seg_usuarios');
			$this->db->join('seg_grupos','fk_grupo_usuario = id_grupo');
			$this->db->where('email_usuario',$valores['email_usuario']);
			$this->db->where('senha_usuario',$valores['senha_usuario']);
			$login = $this->db->get();

			if (isset($login) && !is_null($login) && $login->num_rows() == 1) {
				return $login->row();
			} else {
				return false;
			}

		}
		######################################################	
		//Editar Usuário
		######################################################	
		public function editarUsuario($valores){

			$tabela = "seg_usuarios";
			$id = 'id_usuario';
			
			$this->gerarHistorico($id,$tabela,$valores,$valores[$id]);
			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,$valores);

			return $this->verificarErros($this->db->error(),'Model_webservice / editarUsuario');	


		}
		######################################################	
		//Criar Usuário
		######################################################	
		public function criarUsuario($valores){

			$this->db->insert('seg_usuarios',$valores);
			if($this->verificarErros($this->db->error(),'Model_webservice / criarUsuario')) {
                return $this->db->insert_id();
            } else {
                return false;
            }


		}
		######################################################	
		//Listar Usuários
		######################################################	
		public function listarUsuarios(){

			return $this->db->select('nome_usuario,email_usuario')
					 		->get('seg_usuarios')
					 		->result();


		}

		######################################################	
		//Cadastrar forma de pagamento
		######################################################	
		public function novaFormaPagamento($valores = null){

			$this->db->insert('elo_pagamento_cliente',$valores);

			return $this->verificarErros($this->db->error(),'Model_webservice / novaFormaPagamento');

		}

		public function validarNumCartao($cartao,$id){
			return $this->db->query("select *
										from elo_pagamento_cliente
											where numero_cartao = '{$cartao}'
											  and fk_usuario = {$id}");
		}

		######################################################
		//Listar Formas de pagamento
		######################################################
		public function listarFormasPagamentos($id_usuario){

			return $this->db->query("SELECT
										id_pagamento,
										nome_cartao,
										concat('****.****.****.',right(numero_cartao,4)) as numero_cartao,
										date_format(data_vencimento_cartao,'%m/%Y') as data_vencimento_cartao
									    	from elo_pagamento_cliente
									    		where fk_usuario = {$id_usuario}")->result_array();


		}
		######################################################
		//Editar forma de Pagamento
		######################################################
		public function editarFormaPagamento($valores){

			//Alterar
			$tabela = "elo_pagamento_cliente";
			$id = 'id_pagamento';

			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,$valores);

			return $this->verificarErros($this->db->error(),'Model_webservice / editarFormaPagamento');

		}

		######################################################
		//Listar Produtos
		######################################################
	    public function listarProdutos($id_usuario){
	    	return $this->db->query("select id_produto, produto, round(preco,2) as preco from cad_produtos where fk_usuario is null or fk_usuario = {$id_usuario}")->result_array();
	    }

	    ######################################################
		//Detalhes Produto
		######################################################
	     public function listarDetalhesProduto($produto){
	    	return array(
	    		'produto' => $this->db->query("select cad_produtos.*, round(preco,2) as preco, ifnull((select count(*) from elo_produto_subcategorias where fk_produto = id_produto),0) itens from cad_produtos where id_produto = {$produto['fk_produto']}")->result_array(),

	    		'itens'	=> $this->db->query("
							SELECT 
								fk_subcategoria,
							    concat(categoria,' | ',subcategoria) as opcao
							    	from elo_produto_subcategorias
							        	inner join cad_subcategorias on id_subcategoria = fk_subcategoria
							        	inner join cad_categorias on id_categoria = fk_categoria
							        	where fk_produto = {$produto['fk_produto']}")->result(),
	    	);
	    }

	    ######################################################
		//Listar Categorias
		######################################################
	    public function listarCategorias(){
	    	return $this->db->get("cad_categorias")->result_array();
	    }
	    ######################################################
		//Listar SubCategorias
		######################################################
	    public function listarSubCategorias($fk_categoria){
	    	return $this->db->get_where("cad_subcategorias",array('fk_categoria' => $fk_categoria))
	    					->result_array();
	    }

	    ######################################################
		//Cadastro de lanche
		######################################################
		public function addPedido($valores){
			$this->db->insert('cad_pedidos',$valores);
			if($this->verificarErros($this->db->error(),'Model_webservice / add_pedido')) {
                return $this->db->insert_id();
            } else {
                return false;
            }
		}

		public function inserirItensPedido($id,$item,$quantidade){
			$this->db->query('insert into elo_pedido_itens (fk_item,fk_pedido,quantidade) 
								values ('.$item.','.$id.','.$quantidade.')');
		}

		######################################################
		//Listar Categorias
		######################################################
		public function listarPedidos ($fk_usuario) {
			return $this->db->query("SELECT cad_pedidos.*, 
										date_format(data,'%d/%m/%Y às %H:%i:%s') as data,
										status
										from cad_pedidos
    										inner join cad_status on id_status = fk_status
    											where fk_usuario = {$fk_usuario}")->result_array();
		}
	 	######################################################
		//Listar SubCategorias
		######################################################
		public function listardetalhesPedidos ($id_pedido) {
			return array(

				'pedido' =>  $this->db->query("SELECT cad_pedidos.*, 
										date_format(data,'%d/%m/%Y às %H:%i:%s') as data,
									    status
										from cad_pedidos
    										inner join cad_status on id_status = fk_status
    											where id_pedido = {$id_pedido}")->row_array(),

				'itens' => $this->db->query("SELECT
												id_produto,
											    produto,
											    quantidade
											    	from elo_pedido_itens
											        	inner join cad_pedidos on id_pedido = fk_pedido
											            inner join cad_produtos on id_produto = fk_item
											            where elo_pedido_itens.fk_pedido = {$id_pedido}"
							        )->result_array()
			);
		}
		

	}

?>