<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Controller_pedidos extends CI_Controller {

		function __construct() {

		    parent::__construct();
		    $this->load->model('model_pedidos');
			    
		}

		public function mudar_status(){
			$dados = array( 'fk_status' => $this->input->post('fk_status')+1,
							'id_pedido' => $this->input->post('id_pedido')
					);
			$this->model_pedidos->mudarStatus($dados);
		}

		public function exibir_pedido(){

			$id_pedido = $this->uri->segment(3);

			$detalhes = $this->model_pedidos->exibirPedido($id_pedido);

			echo '<table class="table table-bordered table-hover" align="center">
					<thead>
						<tr>
							<th>Produto</th>
							<th>detalhes</th>
							<th>Quantidade</th>
						</tr>
					</thead>
					<tbody>';

			foreach ($detalhes as $key => $value) {
				echo '<tr>';
				if(is_null($value->usuario)){
					echo "<td>{$value->produto}</td>";
				} else {
					echo "<td>*{$value->produto} <small class=\"label label-info\">Personalizado<small></td>";
				}
				echo "<td>{$value->itens}</td>";
				echo "<td>{$value->quantidade}</td>";
				echo '</tr>';
			}

			echo '</tbody>
			</table>';

		}

	}