<div class="row">
	<div class="col-md-8">
		<h3> <i class="glyphicon glyphicon-stats"></i> Pedidos</h3>
	</div>
</div>

<table class="table table-bordered table-hover" align="center">
	<thead>
		<tr>
			<th class="no-filter">Código</th>
			<th>Cliente</th>
			<th>Preço</th>
			<th>Data</th>
			<th>Status</th>
			<th class="no-filter">Detalhes</th>
		</tr>
	</thead>
	<tbody>


		<?php 

		foreach ($dados_iniciais['pedidos'] as $chave => $pedido) {
			echo "<tr>"; 
			echo "<td align=\"center\">{$pedido['id_pedido']}</td>";
			echo "<td align=\"center\">{$pedido['nome_usuario']}</td>";
			echo "<td align=\"center\">R$ {$pedido['preco']}</td>";
			echo "<td align=\"center\">{$pedido['data_formatada']}</td>";
			switch ($pedido['fk_status']) {
				case '1':
					echo "<td align=\"center\">Aguardando<button class=\"btn btn-info status\" cod=\"{$pedido['fk_status']}\" pedido=\"{$pedido['id_pedido']}\" style=\"width: 100%\">Mudar Para Em Preparo</button></td>";
					break;
				case '2':
					echo "<td align=\"center\">Em Preparo<button class=\"btn btn-success status\" cod=\"{$pedido['fk_status']}\" pedido=\"{$pedido['id_pedido']}\" style=\"width: 100%\">Mudar Para Pronto</button></td>";
					break;
				case '3':
					echo "<td align=\"center\">Pronto<button class=\"btn btn-danger status\" cod=\"{$pedido['fk_status']}\" pedido=\"{$pedido['id_pedido']}\" style=\"width: 100%\">Mudar Para Entregue</button></td>";
					break;
				default:
					echo "<td align=\"center\">Entregue</td>";
					break;
			}
			echo "<td><button class=\"btn btn-info edtForm\" style=\"width: 100%\" data-toggle=\"modal\" data-target=\"#detalhes\" pedido=\"{$pedido['id_pedido']}\">Itens</button></td>";
			echo "</tr>";
		}

		?>
	</tbody>
</table>


<!-- Modal Detalhes -->
<div class="modal fade" id="detalhes" tabindex="-1" role="dialog" aria-labelledby="detalhesLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="detalhesLabel">Detalhes Pedido</h4>
      </div>
      
      	<div class="show"></div>

    </div>
  </div>
</div>


<script type="text/javascript">

	$('.status').click(function(){
		var settings = {
			  "async": true,
			  "crossDomain": true,
			  "url": "<?php echo base_url(); ?>controller_pedidos/mudar_status",
			  "method": "POST",
			  "headers": {
			    "Content-Type": "application/x-www-form-urlencoded",
			    "Cache-Control": "no-cache"
			  },
			  "data": {
			    "fk_status": $(this).attr('cod'),
			    "id_pedido": $(this).attr('pedido')
			  }
			}

			$.ajax(settings).done(function (response) {
			  location.reload();
			});
	})


	$('.edtForm').on('click', function (event) {
	 
	  $('.show').load('<?php echo base_url(); ?>controller_pedidos/exibir_pedido/'+$(this).attr("pedido"), function(){
	  		$("select").select2();
			$('.mascara_monetaria').priceFormat({});
	  });

	});
</script>