<script type="text/javascript">
	history.replaceState({pagina: "lista_grupos"}, "Lista dos Grupos", "<?php echo base_url() ?>main/redirecionar/3");
</script>

<style type="text/css">
	.btn {
		color: white;
		text-shadow: 1px 1px 1px black;
	}
</style>

<div class="row">
	<div class="col-md-6">
		<h1> <i class="glyphicon glyphicon-pencil"></i> Editar Grupo</h1>
	</div>
	<div class="col-md-6" align="right">
		<button type="button" class="btn btn-info" id="voltar"> <i class="glyphicon glyphicon-backward"></i> Voltar</button>
		<button type="button" class="btn btn-info" id="recarregar" url="<?php echo $_SERVER ['REQUEST_URI'] ?>"> <i class="glyphicon glyphicon-refresh"></i> Recarregar</button>
		<button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_historico" id="historico_modal" cod="<?php echo $this->session->flashdata('id_grupo_edicao'); ?>"> <i class="glyphicon glyphicon-time"></i> Histórico Edições</button>
		<a class="btn btn-success" href="<?php echo base_url(); ?>main/redirecionar/6">
			<i class="glyphicon glyphicon-plus-sign"></i> Novo Grupo
		</a>
	</div>
</div>
<hr>

<?php echo form_open('controller_grupos/editar_grupo'); ?>

<input type="hidden" name="grupo_inicial" value="<?php echo $this->session->flashdata('nome_grupo_edicao'); ?>">

<div class="row">
	<div class="col-md-2">
		<div class="form-group has-feedback has-success">
			<label class="control-label" for="id_grupo">ID do Grupo</label> 
			<i class="glyphicon glyphicon-lock form-control-feedback"></i>
			<input type="text" class="form-control" id="id_grupo" name="id_grupo" placeholder="ID do Grupo" value="<?php echo $this->session->flashdata('id_grupo_edicao'); ?>" readonly>
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group has-feedback">
			<label class="control-label" for="nome_grupo">Nome do grupo</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="nome_grupo" name="nome_grupo" placeholder="Nome Grupo" aviso="Nome Grupo" value="<?php echo $this->session->flashdata('nome_grupo_edicao'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group has-feedback">
			<label class="control-label" for="descricao_grupo">Descrição do Grupo</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="descricao_grupo" name="descricao_grupo" placeholder="Descrição do Grupo" aviso="Descrição do Grupo" value="<?php echo $this->session->flashdata('descricao_grupo_edicao'); ?>">
		</div>
	</div>

<?php if ($this->session->flashdata('id_grupo_edicao') == $this->session->userdata("grupo")) { ?>

	<div class="col-md-2" title="Como você pertence a este grupo não pode alterar seu status.">
		<div class="form-group has-feedback">
			<label class="control-label" for="ativo_grupo">Status &#9432;</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<select class="form-control obrigatorio" id="ativo_grupo" name="ativo_grupo" aviso="Status" readonly>
				<option value="1">Ativo</option>
			</select>
		</div>
	</div>

<?php } else { ?>

	<div class="col-md-2">
		<div class="form-group has-feedback">
			<label class="control-label" for="ativo_grupo">Status</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<select class="form-control obrigatorio" id="ativo_grupo" name="ativo_grupo" aviso="Status">
				<option value="1">Ativo</option>
				<option value="0">Inativo</option>
			</select>
		</div>
	</div>

<?php } ?>

	
</div>

<div class="row">
	<div class="col-md-6">
		<h1>Aplicações desse grupo</h1>
	</div>
</div>

<hr>

<?php 
	$controller_atual = 0;
	$aplicacoes_maximo = 0;
	foreach ($dados_iniciais['aplicacoes'] as $chave => $aplicacao): 

		if ($aplicacao->permissao) {
			$checked = "checked";
		} else {
			$checked = "";
		}

		switch ($controller_atual) {

			case 0: //Primeira linha
				$controller_atual = $aplicacao->id_controller;
				$aplicacoes_maximo = 1;
				echo '<div class="row" align="center">
						<div class="col-md-2">
							<small>'.$aplicacao->descricao_controller.'</small>
						</div>
						<div class="col-md-2" title="'.$aplicacao->descricao_aplicacao.'">
							<input type="checkbox" '.$checked.' value="'.$aplicacao->id_aplicacao.'" name="aplicacao[]"> <br> '.$aplicacao->titulo_aplicacao.'
						</div>';
				break;

			case $aplicacao->id_controller: // Faz parte do mesmo grupo

				if ($aplicacoes_maximo == 5) {
					$aplicacoes_maximo = 1;
					echo '</div>
						<hr>
						<div class="row" align="center">
							<div class="col-md-2">
								<small>---</small>
							</div>
							<div class="col-md-2" title="'.$aplicacao->descricao_aplicacao.'">
								<input type="checkbox" '.$checked.' value="'.$aplicacao->id_aplicacao.'" name="aplicacao[]"> <br> '.$aplicacao->titulo_aplicacao.'
							</div>';
				} else {
					$aplicacoes_maximo += 1;
					echo '
						<div class="col-md-2" title="'.$aplicacao->descricao_aplicacao.'">
							<input type="checkbox" '.$checked.' value="'.$aplicacao->id_aplicacao.'" name="aplicacao[]"> <br> '.$aplicacao->titulo_aplicacao.'
						</div>';
				}
				break;

			default: //Segunda linha ou posterior
					$controller_atual = $aplicacao->id_controller;
					$aplicacoes_maximo = 1;
					echo '</div>
						<hr>
						<div class="row" align="center">
							<div class="col-md-2">
								<small>'.$aplicacao->descricao_controller.'</small>
							</div>
							<div class="col-md-2" title="'.$aplicacao->descricao_aplicacao.'">
								<input type="checkbox" '.$checked.' value="'.$aplicacao->id_aplicacao.'" name="aplicacao[]"> <br> '.$aplicacao->titulo_aplicacao.'
							</div>';
				break;
		}

	 endforeach; 

	 echo '</div>'; //fechando a div aberta da última linha

?>

<div class="row finalizar_formulario">
	<div class="col-md-9"></div>
	<div class="col-md-3" align="right">
		<button type="button" class="btn btn-danger" title="Limpar os dados dos campos" id="apagar"> <i class="glyphicon glyphicon-trash"></i> Limpar</button>
		<button type="submit" class="btn btn-success" id="validar_Enviar" title="Salvar Edição"> <i class="glyphicon glyphicon-floppy-disk"></i> Salvar Edição </button>
	</div>
</div>

<?php echo form_close(); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#ativo_grupo').val(<?php echo $this->session->flashdata('ativo_grupo_edicao'); ?>).trigger('change');

		$("#ativo_grupo").change(function(){

			if($(this).val() == 0){
				swal({
				  title: "Lembrete!",
				  text: "Ao Inativar esse grupo TODOS usuários do mesmo irão perder o acesso ao sistema.",
				  type: "warning",
				  timer: 5000,
				  showConfirmButton: false
				});
			}

		});

	});
</script>