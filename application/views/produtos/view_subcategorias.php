<div class="row">
	<div class="col-md-8">
		<h3> <i class="glyphicon glyphicon-stats"></i> Subcategorias</h3>
	</div>

<?php 

if (count($dados_iniciais['categorias']) == 0) {

	echo "<script type=\"text/javascript\">
			alert('Cadastre primeiro uma categoria.');
		</script>";

	
} else {

 ?>

	<div class="col-md-4" align="right">
		<button type="button" class="btn btn-success" href="#" data-toggle="modal" data-target="#addForm">
			<i class="glyphicon glyphicon-plus-sign"></i> Nova Subcategoria
		</button>
	</div>
</div>

<table class="table table-bordered table-hover" align="center">
	<thead>
		<tr>
			<th>Código</th>
			<th>Categoria</th>
			<th>Descrição</th>
      <th>Preço</th>
			<th class="no-filter">Editar</th>
		</tr>
	</thead>
	<tbody>
		<?php 

		foreach ($dados_iniciais['subcategoria'] as $chave => $subcategoria) {
			echo "<tr>"; 
			echo "<td style=\"width: 50px\" align=\"center\">{$subcategoria->id_subcategoria}</td>";
			echo "<td align=\"center\">{$subcategoria->categoria}</td>";
			echo "<td align=\"center\">{$subcategoria->subcategoria}</td>";
      echo "<td align=\"center\" class=\"mascara_monetaria\">{$subcategoria->preco}</td>";
			echo "<td><button cod=\"{$subcategoria->id_subcategoria}\" class=\"btn btn-info edtForm\" style=\"width: 100%\" data-toggle=\"modal\" data-target=\"#editForm\">Editar</button></td>";
			echo "</tr>";
		}

		?>
	</tbody>
</table>


<!-- Modal -->
<div class="modal fade" id="addForm" tabindex="-1" role="dialog" aria-labelledby="addFormLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addFormLabel">Nova Subcategoria</h4>
      </div>
      <div class="modal-body">
        <?php echo form_open('Controller_produtos/criar_subcategoria'); ?>
      	<div class="row">
      		<div class="col-md-4">
      			<label>Categoria</label>
      			<select name="fk_categoria">
      				<?php 
      				foreach ($dados_iniciais['categorias'] as $chave => $categoria) {
      					echo "<option value=\"{$categoria->id_categoria}\">{$categoria->categoria}</option>";
      				}
      				?>
      			</select>
      		</div>

      		<div class="col-md-4">
      			<label>SubCategoria</label>
      			<input type="text" class="form-control" name="subcategoria" placeholder="SubCategoria">
      		</div>

          <div class="col-md-4">
            <label>Preço</label>
            <input type="text" class="form-control mascara_monetaria" name="preco" placeholder="Preço">
          </div>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary" id="validar_Enviar">Cadastrar</button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>



<!-- Modal Editar -->
<div class="modal fade" id="editForm" tabindex="-1" role="dialog" aria-labelledby="editFormLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="editFormLabel">Editar Subcategoria</h4>
      </div>
      
      	<div class="show"></div>

    </div>
  </div>
</div>


<script type="text/javascript">
	$('.edtForm').on('click', function (event) {
	 
	   $('.show').load('<?php echo base_url(); ?>controller_produtos/exibir_subcategoria/'+$(this).attr("cod"), function(){
        $("select").select2();
      $('.mascara_monetaria').priceFormat({});
    });

	});
</script>

<?php } ?>