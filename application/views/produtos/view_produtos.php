<div class="row">
	<div class="col-md-8">
		<h3> <i class="glyphicon glyphicon-stats"></i> Produtos</h3>
	</div>
	<div class="col-md-4" align="right">
		<button type="button" class="btn btn-success" href="#" data-toggle="modal" data-target="#addForm">
			<i class="glyphicon glyphicon-plus-sign"></i> Novo Produto
		</button>
	</div>
</div>

<table class="table table-bordered table-hover" align="center">
	<thead>
		<tr>
			<th class="no-filter">Foto</th>
			<th>Produto</th>
			<th>Preço</th>
			<th>Calorias</th>
			<th>Itens</th>
			<th class="no-filter">Editar</th>
		</tr>
	</thead>
	<tbody>


		<?php 

		foreach ($dados_iniciais['produtos'] as $chave => $produto) {
			echo "<tr>"; 

			$endereco = base_url().'upload/produtos/produto_'.$produto->id_produto;
			echo '<td align=\"center\">
					<img src="'.$endereco.'/produto.png" width="50px" height="50px" title="'.$produto->id_produto.'" onerror="this.src=\''.base_url().'style/img/favicon.ico\';">
				 </td>';
			echo "<td align=\"center\">{$produto->produto}</td>";
			echo "<td align=\"center\">R$ {$produto->preco}</td>";
			echo "<td align=\"center\">{$produto->calorias}</td>";
			echo "<td align=\"center\">{$produto->itens}</td>";
			echo "<td><button cod=\"{$produto->id_produto}\" class=\"btn btn-info edtForm\" style=\"width: 100%\" data-toggle=\"modal\" data-target=\"#editForm\">Editar</button></td>";
			echo "</tr>";
		}

		?>
	</tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="addForm" tabindex="-1" role="dialog" aria-labelledby="addFormLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addFormLabel">Novo Produto</h4>
      </div>
      <div class="modal-body">
        <?php echo form_open_multipart('Controller_produtos/criar_produto',array('id'=>'formImage')); ?>
      	<div class="row">
      		<div class="col-md-3">
  				
  				<img src="<?php echo base_url() ?>style/img/favicon.ico" id="foto" width="80px" style="margin-bottom: 10px;margin-left: 20px;border:none">
	            <p style="width: 10px"> 
	            	<input type="file" id="imagem" name="imagem" width="100"  style="margin-bottom: 20px" onchange="readURL(this,'foto');" />
	            </p>

      		</div>
      		<div class="col-md-3">
      			<label>Produto</label>
      			<input type="text" class="form-control obrigatorio" name="produto" placeholder="Produto">
      		</div>
      		<div class="col-md-3">
      			<label>Calorias</label>
      			<input type="text" class="form-control validar_numeros obrigatorio" name="calorias" placeholder="Calorias">
      		</div>
      		<div class="col-md-3">
      			<label>Preço</label>
      			<input type="text" class="form-control mascara_monetaria obrigatorio" name="preco" placeholder="Preço">
      		</div>
      	</div>

      	<div class="row">
      		<div class="col-md-12">
      			<label>Descrição</label>
      			<textarea name="descricao" style="width: 100%; height: 100px;"></textarea>
      		</div>
      	</div>

      	<div class="row">
			<div class="col-md-12">
				<button type="button" class="btn btn-success" style="width:100%;" id="addItem"><i class="glyphicon glyphicon-plus-sign"></i>Adicionar</button>
			</div>
		</div>

      	<div class="row" id="item">
      		<span>
			<div class="col-md-10">
      			<label>Contém:</label>
      			<select style="width: 100%;" name="item[]">
      				<?php foreach ($dados_iniciais['categorias'] as $chave => $valor) {
      					echo '<option value="'.$valor->id.'">'.$valor->opcao.'</option>';
      				} ?>
      			</select>
      		</div>
      		<div class="col-md-2">
			 	<button type="button" class="btn btn-danger remover" style="margin-top:22px">
			 		<i class="glyphicon glyphicon-trash"></i>
			 	</button>
			 </div>
			 </span>
      	</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="validar_Enviar">Cadastrar</button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>


<!-- Modal Editar -->
<div class="modal fade" id="editForm" tabindex="-1" role="dialog" aria-labelledby="editFormLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="editFormLabel">Editar Categoria</h4>
      </div>
      
      	<div class="show"></div>

    </div>
  </div>
</div>


<script type="text/javascript">

	$(document).on('click','.remover',function () {
        $(this).parent().parent('span').remove();
    });

	var item = $("#item").html();

	$("#addItem").click(function(){
		$("#item").append(item);
		$("select").select2();

	});

	$(document).on('click','#addItemEdit',function(){
		$("#itemEdit").append(item);
		$("select").select2();
	});

	//Exibir imagem ao selecionar.
	function readURL(input, id) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
		    
		    	$('#'+id).attr('src', e.target.result);
			
			}
		 	reader.readAsDataURL(input.files[0]);
		
		}

	}

	$(document).on('click','.enviarEdicao',function(){
		console.log("Editando..");
		$('#formImageEdit').submit();
	});

	$('.edtForm').on('click', function (event) {
	 
	  $('.show').load('<?php echo base_url(); ?>controller_produtos/exibir_produto/'+$(this).attr("cod"), function(){
	  		$("select").select2();
			$('.mascara_monetaria').priceFormat({});
	  });

	});
</script>
