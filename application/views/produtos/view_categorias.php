<div class="row">
	<div class="col-md-8">
		<h3> <i class="glyphicon glyphicon-stats"></i> Categorias</h3>
	</div>
	<div class="col-md-4" align="right">
		<button type="button" class="btn btn-success" href="#" data-toggle="modal" data-target="#addForm">
			<i class="glyphicon glyphicon-plus-sign"></i> Nova Categoria
		</button>
	</div>
</div>

<table class="table table-bordered table-hover" align="center">
	<thead>
		<tr>
			<th>Código</th>
			<th>Categoria</th>
			<th class="no-filter">Editar</th>
		</tr>
	</thead>
	<tbody>
		<?php 

		foreach ($dados_iniciais as $chave => $categoria) {
			echo "<tr>"; 
			echo "<td style=\"width: 100px\" align=\"center\">{$categoria->id_categoria}</td>";
			echo "<td align=\"center\">{$categoria->categoria}</td>";
			echo "<td style=\"width: 100px\"><button cod=\"{$categoria->id_categoria}\" class=\"btn btn-info edtForm\" style=\"width: 100%\" data-toggle=\"modal\" data-target=\"#editForm\">Editar</button></td>";
			echo "</tr>";
		}

		?>
	</tbody>
</table>


<!-- Modal -->
<div class="modal fade" id="addForm" tabindex="-1" role="dialog" aria-labelledby="addFormLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addFormLabel">Nova categoria</h4>
      </div>
      <div class="modal-body">
        <?php echo form_open('Controller_produtos/criar_categoria'); ?>
      	<div class="row">
      		<div class="col-md-12">
      			<label>Categoria</label>
      			<input type="text" class="form-control" name="categoria" placeholder="Categoria">
      		</div>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary">Cadastrar</button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>



<!-- Modal Editar -->
<div class="modal fade" id="editForm" tabindex="-1" role="dialog" aria-labelledby="editFormLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="editFormLabel">Editar Categoria</h4>
      </div>
      
      <?php echo form_open('Controller_produtos/editar_categoria'); ?>
      	<div class="show"></div>
      <?php echo form_close(); ?>

    </div>
  </div>
</div>


<script type="text/javascript">
	$('.edtForm').on('click', function (event) {
	 
	  $('.show').load('<?php echo base_url(); ?>controller_produtos/exibir_categoria/'+$(this).attr("cod"));

	});
</script>