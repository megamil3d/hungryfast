<!DOCTYPE html>
<html lang="pt-br">

  <title><?php echo PROJETO; ?> | Login</title>
  <link rel="shortcut icon" href="<?php echo base_url() ?>style/img/favicon.ico">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link type="text/css" href="<?php echo base_url(); ?>style/css/bootstrap.min.css" rel="stylesheet" />
  <link type="text/css" href="<?php echo base_url(); ?>style/css/login.css" rel="stylesheet" />
  <link type="text/css" href="<?php echo base_url(); ?>style/css/jquery.toast.css" rel="stylesheet" />
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.toast.js"></script>

  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

  <style type="text/css">
    html {
      background-color: #67D5C0;
    }
  </style>

<body>
  <form method="post" action="<?php echo base_url(); ?>main/login">
    
  <div class="container">

    <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-4" align="center">
      
        <img src="<?php echo base_url(); ?>style/img/favicon.ico" width="100" heigth="100">

      </div>
    </div>

    <div class="row" style="margin-top: 20px;">
      <div class="col-md-4"></div>
      <div class="col-md-4">
      
        <div class="input-group">
          <span class="input-group-addon" id="basic-addon1"><span class="fa fa-user"></span></span>
          <input type="text" class="form-control" placeholder="Usuário" aria-describedby="basic-addon1" id="usuario" name="usuario" required>
        </div>

      </div>
    </div>

    <div class="row" style="margin-top: 20px;">
      <div class="col-md-4"></div>
      <div class="col-md-4">
        
         <div class="input-group">
          <span class="input-group-addon" id="basic-addon1"><span class="fa fa-lock"></span></span>
          <input type="password" class="form-control" placeholder="Senha" aria-describedby="basic-addon1" name="senha" required>
        </div>

      </div>
      <div class="col-md-4"></div>

    </div>

    <div class="row" style="margin-top: 20px;">
      <div class="col-md-4"></div>
      <div class="col-md-4">
        
        <span style="width:48%; font-size: 12px; text-align:center;  display: inline-block;"><a class="small-text" id="esqueciSenha" href="#">Esqueceu sua senha?</a></span>

        <button type="submit" class="btn btn-success acessar" style="width:48%;">Entrar</button>

      </div>
      <div class="col-md-4"></div>

    </div>


     <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-4" align="center">
        <div class="progress" id="img_load" style="margin-top: 20px;" hidden>
          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
            <span class="sr-only">Buscando...</span>
          </div>
        </div>
      </div>
    </div>

  </div>

  <div class="row rodape">
      <div class="col-md-3" align="left">
      </div>
      <div class="col-md-6" align="center">
        <a href="promo"><?php echo PROJETO; ?></a>
      </div>
      <div class="col-md-3" align="right">
        Versão 0.1 Alpha
      </div>
  </div> 

</form>

<script type="text/javascript">
  $(document).ready(function(){

  
    var erro_email = false;

    $(".validar_email").focusout(function(){

      console.log('Validando E-mail...');

      if($(this).val() != "") { 

         var filtro = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

         if(!filtro.test($(this).val())) {
    
          toast('E-mail', 'E-mail inválido!','error',true);
          erro_email = true;
          $(this).focus();
          return false;

         } else {

          erro_email = false;
          $.toast().reset('all');

         }

      } else {

        erro_email = false;
        $.toast().reset('all');

      }

   });

    if (!window.Notification) {
          console.log('Habilite as Notificações');
      } else {
          Notification.requestPermission().then(function(p){
              if (p === 'denied') {
                console.log('Notificações Não Aceitas.');
              } else if (p === 'granted') {
                console.log('Notificações Aceitas.');
              }
          });
      }

    $('#esqueciSenha').click(function(){

      if ($('#usuario').val() != "") {

        $('#login').hide();
        $('#img_load').show();

        $.ajax({
              url: "<?php echo base_url(); ?>controller_usuarios/esqueci_Senha",
              type: "post",
              data: {login: $("#usuario").val()},
              datatype: 'json',
              success: function(data){

                if (data['status'] == 1) {
                  toast('Senha alterada com sucesso!',data['resultado'],'success',false);
                } else {
                  toast('Senha não alterada ou erro ao enviar E-mail!',data['resultado'],'error',true);
                }

                 
                $('#login').show();
            $('#img_load').hide();
               

              },
              error:function(){
                toast('Falha','Falha ao alterar senha.','error',true);

                $('#login').show();
                $('#img_load').hide();
                  
              }   
            });

      } else {
        toast('Digite o usuário.','Usuário em branco.','error',true);
      }

    });

    <?php 
      //Caso a tentativa de login falhe
      if(isset($falha)){

        echo "$.toast({
                heading: 'Falha no login',
                text: [
                    'Usuário e/ou senha inválidos', 
                    'Ou Login e/ou Grupo inativo',
                    'Entre em contato com um Administrador.'
                  ],
                hideAfter: false,
                position: 'top-right',
                icon: 'error'
            });";
      }

      //Saiu do sistema.
      if(isset($saiu)){
        //Aviso quando outro login foi realizado
        $forcado_ = "";
        if (isset($forcado) && $forcado != "") {
          $forcado_ = "Novo acesso registrado em: ".$forcado;
        }

        echo "toast('Até logo ".$saiu."!','Acesso encerrado. ".$forcado_."','success',false);";

      }

    ?>

    function toast(titulo,mensagem,tipo,fixo){

      $.toast().reset('all');

      if (fixo) {

         $.toast({
            heading: titulo,
            text: mensagem,
            hideAfter: false,
            position: 'top-right',
            icon: tipo
        });

      } else {

        $.toast({
            heading: titulo,
            text: mensagem,
            showHideTransition: 'fade',
            position: 'top-right',
            hideAfter : 5000,  
            icon: tipo
        });

      } 
    }

  });
  </script>

</body>
</html>