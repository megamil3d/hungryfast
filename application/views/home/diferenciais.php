<link href="https://fonts.googleapis.com/css?family=Fira+Sans" rel="stylesheet">


<div class="container-2" style="padding-left: 0; padding-right: 0;">
    <div class="row" >
        <h1 style="font-family: Fira Sans, sans-serif; color: #000; border-top: 1px solid black; border-bottom: 1px solid black;" class="h1-banner-produtos text-center h1-destaques " >
            COMO FUNCIONA?
        </h1>
    </div>
    <div class="row" id="espaco_branco" style="height: 100px; ">
        <div class="col-lg-3 col-sm-6 col-xs-12 hvr-float-shadow">
            <div class="col-lg-12">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <a href="contato.php"><img src="<?php echo base_url() ?>/home/imagens_tcc/celular.jpg"
                                                class="imagens-destaque img img-rounded img-responsive"
                                                alt="Producao Verticalizada"></a>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="col-lg-12" style="margin-left: 20px;">
                <a href="contato.php" style="text-decoration: none"><h4 style="font-family: Fira Sans, sans-serif; color: #000;"
                                                                         class="text-center h4-destaques">Acesse o<br/>nosso aplicativo.
                    </h4></a>
            </div>
        </div>


        <div class="col-lg-3 col-sm-6 col-xs-12 hvr-float-shadow">
            <div class="col-lg-12">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <a href="contato.php" style="text-decoration: none"><img
                                src="<?php echo base_url() ?>/home/imagens_tcc/ingredientes.jpg"
                                class="imagens-destaque img img-rounded img-responsive"
                                alt="Abs Natural"></a>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="col-lg-12" style="margin-left: 20px;">
                <a href="contato.php" style="text-decoration: none"><h4 style="font-family: Fira Sans, sans-serif; color: #000;"
                                                                         class="text-center h4-destaques">Monte seu<br/>lanche
                    </h4></a>
            </div>
        </div>


        <div class="col-lg-3 col-sm-6 col-xs-12 hvr-float-shadow">
            <div class="col-lg-12">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <a href="contato.php" style="text-decoration: none; color: #000;"><img
                                src="<?php echo base_url() ?>/home/imagens_tcc/confirmar.png"
                                class="imagens-destaque img img-rounded img-responsive" alt="Material Reciclado"></a>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="col-lg-12" style="margin-left: 20px;">
                <a href="contato.php" style="text-decoration: none"><h4 style="font-family: Fira Sans, sans-serif; color: #000;"
                                                                         class="text-center h4-destaques">Confirme o <br/>pedido
                    </h4></a>
            </div>
        </div>

        <div class="col-lg-3 col-sm-6 col-xs-12 hvr-float-shadow">
            <div class="col-lg-12">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <a href="contato.php" style="text-decoration: none"> <img
                                src="<?php echo base_url() ?>/home/imagens_tcc/retirar.jpg"
                                class="imagens-destaque img img-rounded img-responsive" alt="+ de 500 Produtos"></a>
                </div>
                <div class="col-lg-2"></div>
            </div>
          <a  href="categorias.php?categoria=16&filtro=categoria_id">
            <div class="col-lg-12" style="margin-left: 20px;">
               <a href="contato.php" style="text-decoration: none"><h4 style="font-family: Fira Sans, sans-serif; color: #000;"
                                                                           class="text-center h4-destaques">Retire o
                        <br/>lanche
                    </h4></a>
            </div>
          </a>
        </div>


    </div>
</div>

<style>
    @media screen and (min-width: 1199px){
        #slide_show{
            display: none;
        }
    }

    @media screen and (max-width: 768px){
        #slide_show{
            display: table;
            width: 80%;
            margin: 0 auto;
        }
        #espaco_branco{
            display: none;
        }
        #myCarousel2{
            height: 200px;
        }

    }
    @media screen and (max-width: 1199px) and (min-width: 768px){
        #espaco_branco{
            display: none;
        }
        #slide_show{
            padding: 0;
            margin-bottom: 10%;
            display: table;
            width: 80%;
            margin: 0 auto;
        }
        #myCarousel2{
            height: 200px;
        }

    }
</style>


<div id="slide_show">



<div id="myCarousel2" class="carousel slide" data-ride="carousel" style=" " >




    <!-- Indicators -->
    <div class="carousel-inner"  role="listbox" id="slide_100"  style="background: none; ">
        <a href="categorias.php?categoria=16&filtro=categoria_id" style="text-decoration: none;">
        <div class="item active" id="slide_100" style="background: none;">
            <!-- img pequna -->

            <!-- fecha img pequna --><br>
           <div style="text-align: center;"> <h1> <img class="first-slide" src="<?php echo base_url() ?>/home/imagens_tcc/celular.jpg" width="200px" alt="First slide" ></h1></div>

            <a href="processo.php" style="text-decoration: none;"><h4 style="font-family: Fira Sans, sans-serif" class="text-center h4-destaques">Acesse o<br/> nosso aplicativo.

        </div>
        </a>

        <div class="item" style="background: none;">
            <!-- img pequna -->

            <div style="text-align: center;"> <h1> <img class="second-slide" src="<?php echo base_url() ?>/home/imagens_tcc/ingredientes.jpg" width="200px" alt="Second slide" ></h1></div>
            <a href="processo.php" style="text-decoration: none"><h4 style="font-family: Fira Sans, sans-serif"
                                                                     class="text-center h4-destaques">Monte seu<br/>lanche
                </h4></a>
            <!-- fecha img pequna -->

        </div>
        <div class="item" id="slide_100" style="background: none;">
            <!-- img pequna -->

            <div style="text-align: center;"> <h1>  <img src="<?php echo base_url() ?>/home/imagens_tcc/confirmar.png" width="200px"  ></h1></div>
            <a href="processo.php" style="text-decoration: none"><h4 style="font-family: Fira Sans, sans-serif"
                                                                     class="text-center h4-destaques">Confirme o<br/>pedido
                </h4></a>
            <!-- fecha img pequna -->

        </div>
        <div class="item" id="slide_100" style="background: none;">
            <!-- img pequna -->


            <div style="text-align: center;"> <h1> <img src="<?php echo base_url() ?>/home/imagens_tcc/retirar.jpg" width="200px"  ></h1></div>
            <a href="processo.php" style="text-decoration: none"><h4 style="font-family: Fira Sans, sans-serif"
                                                                     class="text-center h4-destaques">Retire o<br/>lanche
                </h4></a>
            <!-- fecha img pequna -->

        </div>
    </div>
    <a class="left carousel-control"  style="background: none; height: 350px;color: #b8347d;opacity: 1"  href="#myCarousel2" role="button" data-slide="prev">
        <span   class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only"  >Previous</span>
    </a>
    <a class="right carousel-control" style="background: none; height: 350px;color: #b8347d;opacity: 1"  href="#myCarousel2" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<!-- /.carousel -->



</div>

<!-- Bootstrap core JavaScript
  ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--
<script src="<?php echo base_url() ?>/home/https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo base_url() ?>/home/../../assets/js/vendor/jquery.min.js"><\/script>')</script> -->
<!--<script src="<?php echo base_url() ?>/home/js/jq1.js"></script>-->
<!--<script src="<?php echo base_url() ?>/home/js/jq3.js"></script>-->
<!--<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<!--<script src="<?php echo base_url() ?>/home/js/jq2.js"></script>-->







