<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="" content="">
    <link rel="icon" href="<?php echo base_url() ?>home/imagens/shekparts_2_FAF-ICON.png">

    <title>HungryFast</title>


    <link href="<?php echo base_url() ?>home/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>home/css/bt2.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>home/css/bt3.css" rel="stylesheet">


    <link href="<?php echo base_url() ?>home/css/ie10-viewport-bug-workaround.css" rel="stylesheet">


    <link href="<?php echo base_url() ?>home/css/starter-template.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>home/css/carousel.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>home/css/estilo.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url() ?>home/css/jquery.bxslider.css">

    <link rel="stylesheet" href="<?php echo base_url() ?>home/css/hover.css">




    <!--    <script src="<?php echo base_url() ?>home/js/.js"></script>-->
    <!--    <script src="<?php echo base_url() ?>home/js/jq2.js"></script>-->
    <!--    <script src="<?php echo base_url() ?>home/js/jq3.js"></script>-->
    <script src="<?php echo base_url() ?>home/js/jquery.js"></script>
<!--    <script src="<?php echo base_url() ?>home/js/jquery.min.js"></script>-->
    <script src="<?php echo base_url() ?>home/js/js1.js"></script>


    <script src="<?php echo base_url() ?>home/js/ie-emulation-modes-warning.js"></script>


    <!-- fontes externas -->


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        #preto {
            float: left;
            position: fixed;
            width: 100%;
            min-height: 100%;
            background: #000;
            z-index: 101;
            opacity: 0;
        }

        #box_branco {
            position: fixed;
            width: 50%;
            display: table;
            height: 20%;
            background: #fff;
            border-radius: 10px;
            margin-right: 50%;
            margin-top: 12.5%;
            margin-left: 25%;
            z-index: 106;
            opacity: 0;

        }

        #outro_branco {
            position: fixed;
            width: 100%;
            display: none;
            height: 60%;
            background: url('imagens/obrigado.png') center center no-repeat;
            background-size: 50%;
            border-radius: 10px;
            margin-right: 50%;
            margin-top: 12.5%;
            z-index: 106;
            opacity: 0;

        }

        @media screen and (max-width: 768px) {
            #outro_branco {
                background-size: 90%;

                margin-top: 20.5%;

            }

            #box_branco {

                margin-top: 30%;
                width: 80%;
                margin-left: 10%;

            }
        }

        #fechar_box {
            padding: 2%;
            font-size: 20px;
            text-align: right;
            margin-right: 3%;
        }

        #fechar_box:hover {
            color: #b8347d;
            cursor: pointer;
        }

        #box_interno {
            margin: -5% 5% 5% 5%
        }

        #colocar_email {
            width: 100%;
            border-radius: 10px;
            padding: 1% 0;
            border: 1px solid #ccc;
        }

        #colocar_email_enviar {
            margin-top: 3%;
            width: 30%;
            display: table;
            border-radius: 10px;
            padding: 1% 0;
            background: #b8347d;
            color: #fff;
            border: 1px solid #b8347d;
        }

        #colocar_email_enviar:hover {
            opacity: .60;
            cursor: pointer;
        }

        #tirar_espaco {
            margin-top: -60px;
        }

        @media screen and (max-width: 581px ) {
            #tirar_espaco {
                margin-top: -80px;
            }
        }


    </style>



</head>

<body>


<script type="text/javascript">

</script>



<?php
	$this->load->view('home/menu');
	$this->load->view('home/carousel');
	$this->load->view('home/banner_produtos');
	$this->load->view('home/diferenciais');
	$this->load->view('home/rodape');
?>
<!---->
<!---->
<!--<!-- Hotjar Tracking Code for www.shekparts_2.com.br -->
<!--<script>-->
<!--    (function (h, o, t, j, a, r) {-->
<!--        h.hj = h.hj || function () {-->
<!--            (h.hj.q = h.hj.q || []).push(arguments)-->
<!--        };-->
<!--        h._hjSettings = {hjid: 599219, hjsv: 5};-->
<!--        a = o.getElementsByTagName('head')[0];-->
<!--        r = o.createElement('script');-->
<!--        r.async = 1;-->
<!--        r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;-->
<!--        a.appendChild(r);-->
<!--    })(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=');-->
<!--</script>-->

<!--<script src="<?php echo base_url() ?>home/js/jquery.min.js"></script>-->
<!--<script src="<?php echo base_url() ?>home/js/jquery-3.1.1.min.js"></script>-->
<!--<script>window.jQuery || document.write('<script src="<?php echo base_url() ?>home/../../assets/js/vendor/jquery.min.js"><\/script>')</script>-->
<script src="<?php echo base_url() ?>home/js/bootstrap.min.js"></script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>-->
<!--<script src="<?php echo base_url() ?>home/js/jquery.bxslider.min.js"></script>-->

</body>

</html>

