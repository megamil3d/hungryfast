<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="<?php echo base_url() ?>/home/imagens/shekparts_2_FAF-ICON.png">

    <title>Parceiros</title>


    <link href="<?php echo base_url() ?>/home/css/bootstrap.css" rel="stylesheet">


    <link href="<?php echo base_url() ?>/home/css/ie10-viewport-bug-workaround.css" rel="stylesheet">


    <link href="<?php echo base_url() ?>/home/css/starter-template.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/home/css/estilo.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/home/css/carousel.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url() ?>/home/css/jquery.bxslider.css">

    <link rel="stylesheet" href="<?php echo base_url() ?>/home/css/hover.css">

    <link href="<?php echo base_url() ?>/home/css/pure.css" rel="stylesheet">



    <script src="<?php echo base_url() ?>/home/js/ie-emulation-modes-warning.js"></script>


    <script src="<?php echo base_url() ?>/home/js/bootstrap.min.js"></script>

    <!-- animacoes teste -->

    <script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- fontes externas -->


    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>

        @media screen and (min-width: 1200px) and (max-width: 1306px) {.baixo-repre-2{height: 100px;}}
        @media screen and (max-width: 482px) {
            #inp {
                width: 90%;
                position: absolute;
            }
            #inp2 {
                width: 90%;
                position: absolute;
            }
        }

        @media screen and (max-width: 768px) {
            #esquerdo {
                width: 100%;
                float: left;
            }

            #direito {
                width: 100%;
                margin-top: 50px;
            }

            #inp2 {
                width: 80%;
            }
            #inp {
                width: 80%;
            }

            #titulo {
                margin-left: 5%;
                margin-right: 16%;
            }
        }

        @media screen and (min-width: 768px) {
            #esquerdo {
                float: right;
                margin-right: 5%;
                width: 60%;
            }

            #titulo {
                margin-left: 18%;
                margin-right: 16%;
            }
        }


    </style>
</head>


<body style="background-color: white; color: #000;">


<?php
include "menu.php";
?>


<br><br><br><br><br>


<script type="text/javascript" src="<?php echo base_url() ?>/home/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>/home/js/jquery.maskedinput.js"></script>



<style>
    @media screen and (min-width: 1921px){
        .input-form{
            width: 60%;
        }
        #corpo_contato{
            margin: 0 auto;width: 1500px;
        }
    }
</style>
<div class="col-lg-12">
    <div id="corpo_contato">







        <br><br>

        <h2 id="titulo" style="color: #000; font-family: 'Open Sans', sans-serif;"><b>Conheça alguns de nossos parceiros</b></h2>
        <br><br>

        <div class="row">

            <div class="col-lg-2"></div>
            <div class="col-lg-4">
                <img src="<?php echo base_url() ?>/home/imagens_tcc/mcdonalds.jpg" alt="" class="img  img-rounded" style="border: 2px solid black" width="300" height="300">
            </div>
            <div class="col-lg-4">
                <h3 style="width: 100%; text-align: center">McDonald's</h3>
                <p>Uma das maiores companhias de FAST FOOD do mundo também está dentro da lista de nossos clientes com 1261 unidades espalhadas em todo o território nacional.</p>
            </div>
            <div class="col-lg-2"></div>


        </div>

        <div class="row" style="margin-top: 10%">

            <div class="col-lg-2"></div>
            <div class="col-lg-4">
                <h3 style="text-align: center; width: 100%;">Burger King</h3>
                <p>Burger King também faz parte da nossa lista de clientes na categoria FAST FOOD com aproximadamente 108 unidades espalhadas em todo o território nacional.</p>
            </div>
            <div class="col-lg-4">
                <img src="<?php echo base_url() ?>/home/imagens_tcc/burgerking.png" alt="" class="img img-rounded" style="border: 2px solid black" width="300" height="300">
            </div>
            <div class="col-lg-2"></div>

        </div>

        <div class="row" style="margin-top: 10%">

            <div class="col-lg-2"></div>
            <div class="col-lg-4">
                <img src="<?php echo base_url() ?>/home/imagens_tcc/subway.jpg" alt="" class="img img-rounded " style="border: 2px solid black; " width="300" height="300">
            </div>
            <div class="col-lg-4">
                <h3 style="text-align: center; width: 100%">Subway</h3>
                <p>
                    A terceira maior companhia de FAST FOOD do Brasil também entra em nossa lista de clientes, com 622 unidades espalhadas pelo Brasil.
                </p>
            </div>
            <div class="col-lg-2"></div>

        </div>

        <div class="row" style="margin-top: 10%">

            <div class="col-lg-2"></div>
            <div class="col-lg-4">
                <h3 style="text-align: center; width: 100%">Bob's</h3>
                <p>
                    A segunda maior compnhia de FAST FOOD também se encontra em nossa lista de clientes, com seus 743 unidades no território nacional.
                </p>
            </div>
            <div class="col-lg-4">
                <img src="<?php echo base_url() ?>/home/imagens_tcc/bobs.jpg" alt="" class="img img-rounded " style="border: 2px solid black" width="300" height="300">
            </div>
            <div class="col-lg-2"></div>

        </div>

        <div class="row" style="margin-top: 10%">

            <div class="col-lg-2"></div>
            <div class="col-lg-4">
                <img src="<?php echo base_url() ?>/home/imagens_tcc/giraffas.png" alt="" class="img img-rounded " style="border: 2px solid black; " width="300" height="300">
            </div>
            <div class="col-lg-4">
                <h3 style="text-align: center; width: 100%">Giraffas</h3>
                <p>
                    A rede giraffas, uma das maiores compnhias de FAST FOOD do Brasil, também está inclusa em nossa lista de clientes, com suas 355 unidades.
                </p>
            </div>
            <div class="col-lg-2"></div>

        </div>


    </div>
</div>

<div style="padding-bottom: 100px"></div>
<div class="baixo-repre-2">
</div>

</body>
<p>&nbsp;</p>
<?php
$this->load->view('home/rodape');
?>
